package continmattia.notefirebase.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import continmattia.notefirebase.R;
import continmattia.notefirebase.adapter.NotesCursorAdapter;
import continmattia.notefirebase.db.NoteHelper;
import continmattia.notefirebase.db.NoteProvider;
import continmattia.notefirebase.dialog.SelectCategoryDialogFragment;
import continmattia.notefirebase.model.Category;
import continmattia.notefirebase.model.Note;
import continmattia.notefirebase.model.User;
import continmattia.notefirebase.utils.NavDrawerWrapper;

public class MainActivity extends SQLiteActivity implements SelectCategoryDialogFragment.OnCategorySelectedListener {

    private static final int KEY_LOGIN_ID = 1;
    private static final String TAG = "MainActivity";
    private static final String NOTES_REF = "notes";

//    private static final int KEY_NOTES_LOADER = 100;

    private User mCurrentUser;
//    private List<Note> mNotesList = new ArrayList<>();

//    private NotesAdapter mNotesAdapter;
//    private LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
    private NotesCursorAdapter mNotesCursorAdapter = new NotesCursorAdapter(this, null);

    private FirebaseDatabase database = FirebaseDatabase.getInstance();

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.notes_lv)
    ListView mListView;
//    @BindView(R.id.notes_rv)
//    RecyclerView mNotesRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        setupListView();
        initNavDrawer(savedInstanceState);

        setupUserSession();
    }

    private void onPostLogin() {
        subscribeToFirebaseNotes();
    }

    private void subscribeToFirebaseNotes() {
        DatabaseReference notesReference = database.getReference(NOTES_REF);

        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                Note note = dataSnapshot.getValue(Note.class);
                Log.i(TAG, "onChildAdded: " + note.getNoteId());
//                if(!mNotesList.contains(note)) {
//                    mNotesList.add(note);
//                    mNotesAdapter.notifyItemInserted(mNotesList.size() - 1);
//                    mLayoutManager.scrollToPosition(mNotesList.size() - 1);
//                }

                saveNewNote(note, dataSnapshot.getKey());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
//                Note editedNote = dataSnapshot.getValue(Note.class);
//                if (mNotesList.contains(editedNote)) {
//                    int index = mNotesList.indexOf(editedNote);
//                    mNotesList.remove(index);
//                    mNotesList.add(index, editedNote);
//                    mNotesAdapter.notifyItemChanged(index);
//                } else {
//                    mNotesList.add(editedNote);
//                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
//                Note note = dataSnapshot.getValue(Note.class);
//                int index = mNotesList.indexOf(note);
//                mNotesList.remove(note);
//                mNotesAdapter.notifyItemRemoved(index);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
//                Note movedComment = dataSnapshot.getValue(Note.class);
//                String commentKey = dataSnapshot.getKey();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "postComments:onCancelled", databaseError.toException());
                Toast.makeText(MainActivity.this, "Failed to load notes.",
                        Toast.LENGTH_SHORT).show();
            }
        };

        notesReference.addChildEventListener(childEventListener);
    }

    private void setupListView() {
        getSupportLoaderManager().initLoader(0, null, this);
        mListView.setAdapter(mNotesCursorAdapter);
    }

//    private void setupRecyclerView() {
//        mLayoutManager = new LinearLayoutManager(this);
//        mNotesRecycler.setLayoutManager(mLayoutManager);
//
//        mNotesAdapter = new NotesAdapter(mNotesList);
//        mNotesRecycler.setAdapter(mNotesAdapter);
//    }

    private void setupUserSession() {
        FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();

        if (mFirebaseUser == null) {
            login();
        } else {
            mCurrentUser = new User(mFirebaseUser.getDisplayName(), mFirebaseUser.getPhotoUrl().toString(), mFirebaseUser.getEmail());
            onPostLogin();
        }
    }

    private void initNavDrawer(Bundle savedInstanceState) {
        if (mCurrentUser == null) {
            mCurrentUser = User.createAnonymous();
        }

        AccountHeader mHeader = NavDrawerWrapper.buildHeaderFromUser(this, mCurrentUser);

        Drawer mDrawer = new DrawerBuilder()
                .withAccountHeader(mHeader)
                .withActivity(this)
                .withToolbar(mToolbar)
                .withActionBarDrawerToggle(true)
                .addDrawerItems(
                        NavDrawerWrapper.makeNotesItem(),
                        NavDrawerWrapper.makeCategoriesItem(),
                        NavDrawerWrapper.makeSettingsItem(),
                        NavDrawerWrapper.makeLogoutItem()
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem != null) {
                            if (drawerItem instanceof Nameable) {
                                String name = ((Nameable) drawerItem).getName().getText(MainActivity.this);
                                mToolbar.setTitle(name);
                            }
                            onTouchDrawer(position);
                        }
                        return false;
                    }
                })
                .withOnDrawerListener(new Drawer.OnDrawerListener() {

                    @Override
                    public void onDrawerOpened(View drawerView) {

                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {

                    }

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {

                    }
                })
                .withFireOnInitialOnClick(true)
                .withSavedInstance(savedInstanceState)
                .build();

        mDrawer.addStickyFooterItem(NavDrawerWrapper.makeDeleteAccountItem());
    }

    private void onTouchDrawer(int identifier) {
        switch (identifier) {
            case NavDrawerWrapper.ID_NOTES:
                break;
            case NavDrawerWrapper.ID_CATEGORIES:
                break;
            case NavDrawerWrapper.ID_SETTINGS:
                break;
            case NavDrawerWrapper.ID_LOGOUT:
//                logout();
                break;
            case NavDrawerWrapper.ID_DELETE_ACCOUNT:
//                deleteAccount();
                break;
        }
    }

    private void logout() {
        AuthUI.getInstance().signOut(this).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    login();
                } else {
                    Toast.makeText(MainActivity.this, "Signout failed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void deleteAccount() {
        AuthUI.getInstance().delete(this).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    login();
                } else {
                    Toast.makeText(MainActivity.this, "Delete failed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void login() {
        startActivityForResult(AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(
                                Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                                        new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build())).build(),
                KEY_LOGIN_ID);
    }

    private void showSnackbar(@StringRes int stringId) {
        Snackbar.make(mToolbar, stringId, Snackbar.LENGTH_SHORT).show();
    }

    @OnClick(R.id.fab)
    void onFabClicked() {
        Note note = Note.makeRandomNote();

        DatabaseReference notesReference = database.getReference(NOTES_REF);

        DatabaseReference noteReference = notesReference.push();
        note.setNoteId(noteReference.getKey());
        noteReference.setValue(note);

//        mNotesList.add(note);
//        mNotesAdapter.notifyItemInserted(mNotesList.size() - 1);
//        mLayoutManager.scrollToPosition(mNotesList.size() - 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == KEY_LOGIN_ID) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                onPostLogin();
                return;
            } else {
                if (response == null) {
                    showSnackbar(R.string.error_login_cancelled);
                    return;
                }

                if (response.getErrorCode() == ErrorCodes.NO_NETWORK) {
                    showSnackbar(R.string.error_no_network);
                    return;
                }

                if (response.getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                    showSnackbar(R.string.error_unknown);
                    return;
                }
            }

            showSnackbar(R.string.error_unknown_response);
        }
    }

    @Override
    protected void saveNewNote(Note note, String firebaseId) {
        ContentValues values = new ContentValues();

        values.put(NoteHelper.COL_CID, firebaseId);
        values.put(NoteHelper.COL_TITLE, note.getTitle());
        values.put(NoteHelper.COL_CONTENT, note.getContent());

        // content provider definito nel manifest
        getContentResolver().insert(NoteProvider.NOTES_URI, values);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, NoteProvider.NOTES_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mNotesCursorAdapter.swapCursor(cursor);
    }

    @Override
    public void onCategorySelected(Category category) {

    }
}
