package continmattia.notefirebase.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import continmattia.notefirebase.R;
import continmattia.notefirebase.db.NoteHelper;

public class CategoriesCursorAdapter extends CursorAdapter {

    public CategoriesCursorAdapter(Context context, Cursor c) {
        super(context, c, false);
    }

    private class ViewHolder {
        TextView mTitle;
        TextView mExcerpt;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.card_note, null);

        ViewHolder holder = new ViewHolder();
        holder.mTitle = (TextView) view.findViewById(R.id.note_title_tv);
        holder.mExcerpt = (TextView) view.findViewById(R.id.note_excerpt_tv);
        view.setTag(holder);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.mTitle.setText("" + cursor.getString(cursor.getColumnIndex(NoteHelper.COL_TITLE)));

        String excerpt = cursor.getString(cursor.getColumnIndex(NoteHelper.COL_CONTENT)).substring(0, 60) + "...";
        holder.mExcerpt.setText(excerpt);
    }
}
