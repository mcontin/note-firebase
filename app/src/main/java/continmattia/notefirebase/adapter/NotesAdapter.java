package continmattia.notefirebase.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import continmattia.notefirebase.R;
import continmattia.notefirebase.model.Note;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> {

    private List<Note> mData;

    public NotesAdapter(List<Note> mNotesList) {
        mData = mNotesList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_note, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Note currentNote = mData.get(position);
        String strOut = currentNote.getContent();
        String excerpt = strOut.substring(0, 60) + "...";

        holder.mTitle.setText(currentNote.getTitle());
        holder.mExcerpt.setText(excerpt);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.note_title_tv)
        TextView mTitle;
        @BindView(R.id.note_excerpt_tv)
        TextView mExcerpt;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
