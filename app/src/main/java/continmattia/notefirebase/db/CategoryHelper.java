package continmattia.notefirebase.db;

import android.provider.BaseColumns;

public class CategoryHelper implements BaseColumns {

    public static final String TABLE_NAME = "category";

    // lap duration in seconds
    public static final String COL_CATEGORY_NAME = "duration";

    public static final String CREATE_QUERY =
            "CREATE TABLE " + TABLE_NAME + " ( " +
                    _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COL_CATEGORY_NAME + " INTEGER NOT NULL " +
                    ");";

    public static final String DROP_QUERY =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

}
