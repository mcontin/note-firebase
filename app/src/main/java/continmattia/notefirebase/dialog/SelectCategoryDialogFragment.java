package continmattia.notefirebase.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import continmattia.notefirebase.R;
import continmattia.notefirebase.adapter.CategoryAdapter;
import continmattia.notefirebase.model.Category;

public class SelectCategoryDialogFragment extends DialogFragment {

    private List<Category> mCategories;
    private CategoryAdapter mCategoryAdapter;
    private OnCategorySelectedListener mCategorySelectedListener;

    public interface OnCategorySelectedListener {
        void onCategorySelected(Category category);
    }

    public SelectCategoryDialogFragment() {
        // Required empty public constructor
    }

    public static SelectCategoryDialogFragment newInstance(List<Category> categories){
        SelectCategoryDialogFragment dialog = new SelectCategoryDialogFragment();
        dialog.mCategories = categories;
        return dialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCategorySelectedListener) {
            mCategorySelectedListener = (OnCategorySelectedListener) context;
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View convertView = inflater.inflate(R.layout.category_dialog_list, null);
        builder.setView(convertView);

//        View titleView = inflater.inflate(R.layout.choose_category_dialog_title, null);
//        builder.setCustomTitle(titleView);
        builder.setTitle("Choose Category");

        ListView dialogList = (ListView) convertView.findViewById(R.id.dialog_listview);
        TextView emptyText = (TextView) convertView.findViewById(R.id.category_list_empty);
        dialogList.setEmptyView(emptyText);

        mCategoryAdapter = new CategoryAdapter(getActivity(), mCategories);
        dialogList.setAdapter(mCategoryAdapter);

        dialogList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Category mSelectedCategory = mCategories.get(position);
                if (mSelectedCategory != null){
                    mCategorySelectedListener.onCategorySelected(mSelectedCategory);
                }
            }
        });

        return builder.create();
    }

}